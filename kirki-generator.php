<?php
/*
Plugin Name: Kirki Generator
*/

define( 'KIRKI_GEN_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( KIRKI_GEN_PLUGIN_DIR . 'includes/post-type.php' );
require_once( KIRKI_GEN_PLUGIN_DIR . 'includes/class-kirki-generator.php' );

add_action( 'init', function() {
	$init_kirki_generator = Kirki_Generator::get_instance();
});
