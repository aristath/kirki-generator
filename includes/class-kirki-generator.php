<?php

class Kirki_Generator {

	public static $instance = null;

	public static $content = '';


	public function __construct() {
		add_filter( 'the_content', array( $this, 'build_content' ) );
	}

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new Kirki_Generator();
		}
		return self::$instance;
	}

	public function build_content( $content ) {
		if ( is_singular( 'recipe' ) ) {
			/**
			 * Create the config
			 */
			self::comment( 'Create the Kirki config for ' . esc_attr( get_field( 'config_id' ) ) );
			self::content( "Kirki::add_config( '" . esc_attr( get_field( 'config_id' ) ) . "', array(" );
			$config = array(
				'capability'  => "'" . esc_attr( get_field( 'capability' ) ) . "'",
				'option_type' => "'" . esc_attr( get_field( 'option_type' ) ) . "'",
			);
			if ( 'option' == get_field( 'option_type' ) && get_field( 'serialized_options' ) ) {
				$config['option_name'] = "'" . esc_attr( get_field( 'option_name' ) ) . "'";
			}
			self::analyse_array( $config );
			self::content( ') );' );
			self::content();

			/**
			 * Create Panels
			 */
			if ( have_rows( 'panels' ) ) {
				self::comment( 'Create Panels' );
				while ( have_rows( 'panels' ) ) : the_row();
					self::content( "Kirki::add_panel( '" . esc_attr( get_sub_field( 'panel_id' ) ) . "', array(" );
					$panel = array(
						'priority' => intval( get_sub_field( 'priority' ) ),
						'title'    => "esc_html__( '" . esc_textarea( get_sub_field( 'title' ) ) . "', '" . esc_attr( get_field( 'textdomain' ) ) . "' )",
					);
					if ( ! empty( trim( get_sub_field( 'description' ) ) ) ) {
						$panel['description'] = "( '" . esc_textarea( get_sub_field( 'description' ) ) . "', '" . esc_attr( get_field( 'textdomain' ) ) . "' )";
					}
					self::analyse_array( $panel );
					self::content( ') );' );
					self::content();
				endwhile;
			}

			/**
			 * Create Sections
			 */
			if ( have_rows( 'sections' ) ) {
				self::comment( 'Create Sections' );
				while ( have_rows( 'sections' ) ) : the_row();
					self::content( "Kirki::add_section( '" . esc_attr( get_sub_field( 'section_id' ) ) . "', array(" );
					$section = array(
						'priority' => intval( get_sub_field( 'priority' ) ),
						'title'    => "esc_html__( '" . esc_textarea( get_sub_field( 'title' ) ) . "', '" . esc_attr( get_field( 'textdomain' ) ) . "' )",
					);
					if ( ! empty( trim( get_sub_field( 'description' ) ) ) ) {
						$section['description'] = "esc_html__( '" . esc_textarea( get_sub_field( 'description' ) ) . "', '" . esc_attr( get_field( 'textdomain' ) ) . "' )";
					}
					if ( ! empty( trim( get_sub_field( 'panel' ) ) ) ) {
						$section['panel'] = "'" . esc_attr__( get_sub_field( 'panel' ) ) . "'";
					}
					self::analyse_array( $section );
					self::content( ') );' );
					self::content();
				endwhile;
			}

			/**
			 * Create Fields
			 */
			if ( have_rows( 'fields' ) ) {
				self::comment( 'Create Fields' );
				while ( have_rows( 'fields' ) ) : the_row();
					self::content( "Kirki::add_field( '" . esc_attr( get_field( 'config_id' ) ) . "', array(" );
					$field = array(
						'priority' => intval( get_sub_field( 'priority' ) ),
						'label'    => "esc_html__( '" . esc_textarea( get_sub_field( 'label' ) ) . "', '" . esc_attr( get_field( 'textdomain' ) ) . "' )",
						'type'     => "'" . esc_attr( get_sub_field( 'type' ) ) . "'",
					);
					if ( ! empty( trim( get_sub_field( 'description' ) ) ) ) {
						$field['description'] = "esc_html__( '" . esc_textarea( get_sub_field( 'description' ) ) . "', '" . esc_attr( get_field( 'textdomain' ) ) . "' )";
					}

					// Create choices for select & radios
					if ( in_array( get_sub_field( 'type' ), array( 'select', 'radio', 'radio-buttonset', 'radio-image' ) ) ) {
						$field['choices'] = array();
						while ( have_rows( 'choices' ) ) : the_row();
							$field['choices'][ (string) esc_attr( get_sub_field( 'key' ) ) ] = "esc_html__( '" . esc_textarea( get_sub_field( 'label' ) ) . "', '" . esc_attr( get_field( 'textdomain' ) ) . "' )";
						endwhile;
					} elseif ( in_array( get_sub_field( 'type' ), array( 'palette' ) ) ) {
						$field['choices'] = array();
						while ( have_rows( 'choices_palette' ) ) : the_row();
							$key = esc_attr( get_sub_field( 'key' ) );
							$field['choices'][ $key ] = array();
							$i = 0;
							while ( have_rows( 'colors' ) ) : the_row();
								$field['choices'][ $key ][ $i ] = "'" . get_sub_field( 'color' ) . "'";
								$i++;
							endwhile;
						endwhile;
					}

					// Default values
					if ( in_array( get_sub_field( 'type' ), array( 'checkbox', 'switch', 'toggle' ) ) ) {
						$field['default'] = "'" . (int) get_sub_field( 'default_checkbox' ) . "'";
					} elseif ( in_array( get_sub_field( 'type' ), array( 'text', 'textarea', 'editor' ) ) ) {
						$field['default'] = "'" . get_sub_field( 'default_text' ) . "'";
					} elseif ( in_array( get_sub_field( 'type' ), array( 'color' ) ) ) {
						$field['default'] = "'" . get_sub_field( 'default_color' ) . "'";
					} elseif ( in_array( get_sub_field( 'type' ), array( 'color-alpha' ) ) ) {
						$field['default'] = "'" . str_replace( ' ', '', get_sub_field( 'default_color_alpha' ) ) . "'";
					} elseif ( in_array( get_sub_field( 'type' ), array( 'select' ) ) ) {
						if ( 'yes' != get_sub_field( 'multiselect' ) ) {
							$field['default'] = "'" . get_sub_field( 'default_select' ) . "'";
						} else {
							$default_choices = explode( ',', get_sub_field( 'default_multiselect' ) );
							$sanitized_default_choices = array();
							foreach ( $default_choices as $default_choice ) {
								$default_choice = esc_attr( trim( $default_choice ) );
								if ( array_key_exists( $default_choice, $choices ) ) {
									$sanitized_default_choices[] = "'" . $default_choice . "'";
								}
							}
							$field['default'] = $sanitized_default_choices;
						}
					} elseif ( in_array( get_sub_field( 'type' ), array( 'radio', 'radio-buttonset', 'radio-image', 'palette' ) ) ) {
						$field['default'] = "'" . str_replace( ' ', '', get_sub_field( 'default_select' ) ) . "'";
					}

					// Output
					if ( have_rows( 'output' ) ) {
						$field['output'] = array();
						if ( 'yes' == get_sub_field( 'auto_postmessage' ) ) {
							$field['transport'] = "'postMessage'";
							$field['js_vars']   = array();
						}
						$i = 0;
						while ( have_rows( 'output' ) ) : the_row();
							if ( 'yes' == get_sub_field( 'auto_postmessage' ) ) {
								$field['js_vars'][ $i ]['function'] = "'css'";
							}
							if ( ! empty( trim( get_sub_field( 'element' ) ) ) ) {
								$field['output'][ $i ]['element'] = "'" . esc_attr( get_sub_field( 'element' ) ) . "'";
								if ( 'yes' == get_sub_field( 'auto_postmessage' ) ) {
									$field['js_vars'][ $i ]['element'] = "'" . esc_attr( get_sub_field( 'element' ) ) . "'";
								}
							}
							if ( ! empty( trim( get_sub_field( 'property' ) ) ) ) {
								$field['output'][ $i ]['property'] = "'" . esc_attr( get_sub_field( 'property' ) ) . "'";
								if ( 'yes' == get_sub_field( 'auto_postmessage' ) ) {
									$field['js_vars'][ $i ]['property'] = "'" . esc_attr( get_sub_field( 'property' ) ) . "'";
								}
							}
							if ( ! empty( trim( get_sub_field( 'units' ) ) ) ) {
								$field['output'][ $i ]['units'] = "'" . esc_attr( get_sub_field( 'units' ) ) . "'";
								if ( 'yes' == get_sub_field( 'auto_postmessage' ) ) {
									$field['js_vars'][ $i ]['units'] = "'" . esc_attr( get_sub_field( 'units' ) ) . "'";
								}
							}
							if ( ! empty( trim( get_sub_field( 'prefix' ) ) ) ) {
								$field['output'][ $i ]['prefix'] = "'" . esc_attr( get_sub_field( 'prefix' ) ) . "'";
								if ( 'yes' == get_sub_field( 'auto_postmessage' ) ) {
									$field['js_vars'][ $i ]['prefix'] = "'" . esc_attr( get_sub_field( 'prefix' ) ) . "'";
								}
							}
							if ( ! empty( trim( get_sub_field( 'suffix' ) ) ) ) {
								$field['output'][ $i ]['suffix'] = "'" . esc_attr( get_sub_field( 'suffix' ) ) . "'";
								if ( 'yes' == get_sub_field( 'auto_postmessage' ) ) {
									$field['js_vars'][ $i ]['suffix'] = "'" . esc_attr( get_sub_field( 'suffix' ) ) . "'";
								}
							}
							$i++;
						endwhile;
					}
					self::analyse_array( $field );
					self::content( ') );' );
					self::content();
				endwhile;
			}
			return '[code lang="php"]' . self::$content . '[/code]';
		}
		return $content;
	}

	public static function content( $content = '', $new_line = true ) {
		self::$content .= $content;
		if ( $new_line ) {
			self::$content .= self::new_line();
		}
	}

	public static function analyse_array( $args, $depth = 0, $force_keys = false ) {
		$depth = ( 1 > $depth ) ? 1 : $depth;
		foreach ( $args as $arg_key => $arg_value ) {
			if ( is_string( $arg_key ) ) {
				$arg_key = esc_attr( trim( $arg_key ) );
			} elseif ( $force_keys && is_int( $arg_key ) ){
				$arg_key = (string) $arg_key;
			}
			// indent
			self::$content .= str_repeat( '	', $depth );
			// Add key + spaces + =>
			if ( is_string( $arg_key ) || ( is_int( $arg_key ) && $force_keys ) ) {
				self::$content .= '\'' . $arg_key . '\'' . str_repeat( ' ', self::max_key_strlen( $args ) - strlen( $arg_key ) + 1 ) . '=> ';
			}
			if ( ! is_array( $arg_value ) ) {
				self::$content .= self::new_line( $arg_value . ',' );
			} else {
				self::$content .= self::new_line( 'array(' );
				if ( 'choices' == $arg_key ) {
					self::analyse_array( $arg_value, $depth + 1, true );
				} else {
					self::analyse_array( $arg_value, $depth + 1 );
				}
				self::$content .= self::new_line( str_repeat( '	', $depth ) . '),' );
			}
		}
	}

	/**
	 * Figure out the maximum string length of the array keys.
	 */
	public static function max_key_strlen( $array = array() ) {
		$max_key_strlen = 0;
		foreach ( $array as $key => $value ) {
			if ( $max_key_strlen < strlen( esc_attr( trim( $key ) ) ) ) {
				$max_key_strlen = strlen( esc_attr( trim( $key ) ) );
			}
		}
		return $max_key_strlen;
	}

	/**
	 * Return a PHP comment
	 */
	public static function comment( $content ) {
		return self::new_line( '/**' ) . self::new_line( ' * ' . $content ) . self::new_line( ' */' );
	}

	/**
	 * Content + new line characters.
	 */
	public static function new_line( $content = '' ) {
		return $content . "\r\n";
	}
}
